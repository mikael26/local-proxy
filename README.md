# Local Proxy

This projects help you run multiple development environments using port 80 and 443 at the same time

## How to use

Update your local hosts file with the desired hostnames
and modify your http container to look something like this:
```yaml
version: '3.5'
services:
  app-webserver:
    image: nginx:1.10
    environment:
      VIRTUAL_HOST: project.client.localhost,project-alias.client.localhost
      SELF_SIGNED_HOST: project.client.localhost,project-alias.client.localhost
      VIRTUAL_PORT: 80
    networks:
      - default
      - proxy
networks:
  default:
  proxy:
    name: proxy
```

Add this as an executable bash-script somewhere in your project
```bash
#!/bin/sh
DIR=~/local-proxy/
if test -f "$DIR/README.md"; then
    echo "$DIR exists. Updating..."
    cd $DIR
    git pull
else
    echo "$DIR does not exists. Installing..."
    git clone git@gitlab.com:mikael26/local-proxy.git $DIR
    cd $DIR
fi
sh ./bin/up
```

